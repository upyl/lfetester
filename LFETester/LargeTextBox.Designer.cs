﻿namespace LFETester
{
    partial class LargeTextBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.vscroll = new System.Windows.Forms.VScrollBar();
            this.rtb = new LFETester.RichTextBoxEx();
            this.SuspendLayout();
            // 
            // vscroll
            // 
            this.vscroll.Dock = System.Windows.Forms.DockStyle.Right;
            this.vscroll.Location = new System.Drawing.Point(409, 0);
            this.vscroll.Name = "vscroll";
            this.vscroll.Size = new System.Drawing.Size(17, 355);
            this.vscroll.TabIndex = 0;
            this.vscroll.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vscroll_Scroll);
            // 
            // rtb
            // 
            this.rtb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtb.Location = new System.Drawing.Point(0, 0);
            this.rtb.Name = "rtb";
            this.rtb.NumberAlignment = System.Drawing.StringAlignment.Center;
            this.rtb.NumberBackground1 = System.Drawing.SystemColors.ControlLight;
            this.rtb.NumberBackground2 = System.Drawing.SystemColors.Window;
            this.rtb.NumberBorder = System.Drawing.SystemColors.ControlDark;
            this.rtb.NumberBorderThickness = 1F;
            this.rtb.NumberColor = System.Drawing.Color.DarkGray;
            this.rtb.NumberFont = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtb.NumberLeadingZeroes = false;
            this.rtb.NumberLineCounting = LFETester.RichTextBoxEx.LineCounting.CRLF;
            this.rtb.NumberPadding = 2;
            this.rtb.ShowLineNumbers = true;
            this.rtb.Size = new System.Drawing.Size(409, 355);
            this.rtb.TabIndex = 1;
            this.rtb.Text = "";
            // 
            // LargeTextBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.rtb);
            this.Controls.Add(this.vscroll);
            this.Name = "LargeTextBox";
            this.Size = new System.Drawing.Size(426, 355);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.VScrollBar vscroll;
        private RichTextBoxEx rtb;
    }
}
