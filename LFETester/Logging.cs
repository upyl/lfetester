﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace LFETester
{
    class Logging
    {
        static StreamWriter sw = null;

        public static void Initialize()
        {
            if (File.Exists("log.txt"))
                File.Delete("log.txt");
        }

        public static void Log(string message)
        {
            sw = new StreamWriter("log.txt", true);
            sw.WriteLine(message);
            sw.Close();


        }
    }
}
