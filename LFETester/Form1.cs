﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LFETester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            largeTextBox1.PositionChanged += new LargeTextBox.PositionChangeHandler(largeTextBox1_PositionChanged);
        }

        void largeTextBox1_PositionChanged(object sender, string pos)
        {
            label1.Text = pos;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            largeTextBox1.LoadFile(textBox2.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            largeTextBox1.SaveProgress += new ProgressChanged(largeTextBox1_SaveProgress);
            largeTextBox1.SaveFile(textBox3.Text); 
        }

        void largeTextBox1_SaveProgress(object sender, int progress)
        {
            this.Invoke((MethodInvoker)delegate()
            {
                label3.Text = "Saving:  " + progress + " %";
            });
        }

        private void button1_Click(object sender, EventArgs e)
        {
            largeTextBox1.SearchProgress += new LargeTextBox.SearchProgressHandler(largeTextBox1_SearchProgress);
            largeTextBox1.SearchTermFound += new LargeTextBox.SearchFoundHandler(largeTextBox1_SearchTermFound);
            largeTextBox1.Search(textBox1.Text);
        }

        void largeTextBox1_SearchProgress(object sender, int percent)
        {

            this.Invoke((MethodInvoker)delegate()
            {
                label2.Text = "Searching " + percent + " %";
            });
        }

        void largeTextBox1_SearchTermFound(object sender, long position)
        {
            this.Invoke((MethodInvoker)delegate()
            {
                label2.Text = "Found at: " + position;
            });

        }

    }
}
