﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace LFETester
{
    public partial class LargeTextBox : UserControl
    {
        #region Internal variables
        int NUMBER_CACHED_LINES; // How many lines need to be cached
        int RTB_HEIGHT; // Height of rich text box corresponding to lines cached
        int MAX_CHARS_PER_LINE = 2048; // Maximum number of characters per line
        char[][] displayedLines; // The list of currently displayed text lines
        int[] lineLengths; // The list of lengths of currently displayed text lines
        string filePath; // The path the the file being edited

        RevisionStream fileRead; // The revision stream on which the RTB is based

        long topPosition; // The position in the changed stream of first line in RTB       
        long topPositionBeforeScroll; // Backup of topPosition used for detecting diffs after scrolling    

        bool dirty; // True if RTB changed since last Cache
        bool suspendTextChanged; // If True, RTB text changed event is ignored
        bool fileInitialized; // True if file loaded

        long currentSearchPosition; // The position where the current search should start
        String currentSearchTerm; // The term we are currently searching for

        #endregion

        public LargeTextBox()
        {
            InitializeComponent();

            rtb.MouseWheel += new MouseEventHandler(rtb_MouseWheel);
        }

        #region Internal Methods
        private void InitializeLines()
        {
            if (rtb.Height == RTB_HEIGHT)
                return;
            RTB_HEIGHT = rtb.Height;

            String savedText = rtb.Text;
            int selStart = rtb.SelectionStart;
            int selLen = rtb.SelectionLength;

            rtb.Text = new String('\n', 1024);
            int lastLn = rtb.GetLineFromCharIndex(rtb.GetCharIndexFromPosition(new Point(rtb.Width - 1, rtb.Height - 1)));
            NUMBER_CACHED_LINES = lastLn + 1;
            lineLengths = new int[NUMBER_CACHED_LINES];
            displayedLines = new char[NUMBER_CACHED_LINES][];
            for (int i = 0; i < NUMBER_CACHED_LINES; ++i)
                displayedLines[i] = new char[MAX_CHARS_PER_LINE];

            rtb.Text = savedText;
            rtb.SelectionStart = selStart;
            rtb.SelectionLength = selLen;
            dirty = false;
        }

        private void CacheLines()
        {
            if (dirty)
            {
                SynchDifferences();
                dirty = false;
            }
            int crtLine = 0;

            while (crtLine < NUMBER_CACHED_LINES && topPosition < fileRead.Length)
            {
                int newP;
                String ln = ReadLine(out newP);


                Array.Copy(ln.ToCharArray(), displayedLines[crtLine], ln.Length);
                if (ln.Length == 1024)
                    Console.Write("s");

                lineLengths[crtLine] = ln.Length;
                // sb.AppendLine(ln);
                crtLine++;
                fileRead.Position = newP;
            }

        }
        private void RefreshText()
        {
            StringBuilder sb = new StringBuilder();


            for (int i = 0; i < NUMBER_CACHED_LINES; ++i)
            {
                string s = new string(displayedLines[i], 0, lineLengths[i]);

                if (s.EndsWith("\r\n"))
                    s = s.Substring(0, s.Length - 2);
                else
                    if (s.EndsWith("\r") || s.EndsWith("\n"))
                        s = s.Substring(0, s.Length - 1);


                if (i == NUMBER_CACHED_LINES - 1)
                    sb.Append(s);
                else
                    sb.AppendLine(s);
            }


            rtb.Text = sb.ToString(); // This sets dirty to true
            dirty = false;
        }
        private String ReadLine(out int newPos)
        {
            byte[] buf = new byte[2048];

            int rd = fileRead.Read(buf, (int)fileRead.Position, 2048);
            newPos = (int)fileRead.Position;

            string res = "";
            if (rd > 0)
            {


                char[] cbuf = fileRead.DetectedEncoding.GetChars(buf);
                res = new string(cbuf);
                int idxr = res.IndexOf("\r");
                int idxn = res.IndexOf("\n");

                if (idxr == -1 && idxn == -1)
                    return res;

                int finalPos;

                if (idxn == -1)
                {
                    if (idxr == -1)
                    {
                        //Weird? need to cache more per line
                        throw new Exception("No newline on this line");
                    }
                    else
                    {
                        //Only have one \r
                        res = res.Substring(0, idxr + 1);
                        finalPos = (idxr + 1) * fileRead.CHAR_WIDTH;
                    }
                }
                else
                {
                    if (idxr == -1)
                    {
                        //Only have one \n
                        res = res.Substring(0, idxn + 1);
                        finalPos = (idxn + 1) * fileRead.CHAR_WIDTH;
                    }
                    else
                    {
                        //Have both \r and \n
                        if (idxn == idxr + 1)
                        {
                            //\r\n
                            res = res.Substring(0, idxn + 1);
                            finalPos = (idxn + 1) * fileRead.CHAR_WIDTH;
                        }
                        else
                        {
                            if (idxr < idxn)
                            {  // \r ... futher away \n
                                res = res.Substring(0, idxr + 1);
                                finalPos = (idxr + 1) * fileRead.CHAR_WIDTH;
                            }
                            else
                            {
                                // \n ... further away \r
                                res = res.Substring(0, idxn + 1);
                                finalPos = (idxn + 1) * fileRead.CHAR_WIDTH;
                            }

                        }

                    }
                }


                newPos = (int)fileRead.Position + finalPos;
                fileRead.Position = newPos;
            }
            return res;
        }
        private void AdvanceOneLine()
        {
            TopPosition = topPosition + lineLengths[0] * fileRead.CHAR_WIDTH;
            fileRead.Position = topPosition;
        }
        private void BackOneLine()
        {
            if (topPosition == 0)
                return;

            //Read back
            long attemptPos = topPosition - 2048;
            if (attemptPos < 0)
                attemptPos = 0;

            fileRead.Position = attemptPos;
            byte[] buf = new byte[2048];
            int rd = fileRead.Read(buf, (int)fileRead.Position, (int)(topPosition - attemptPos));
            fileRead.Position += rd;

            char[] cbuf = fileRead.DetectedEncoding.GetChars(buf);
            String res = new string(cbuf);

            int k = rd / fileRead.CHAR_WIDTH - 1;
            int nrSep = 0;
            while (k > 0 && nrSep < 2)
            {
                if (cbuf[k] == '\r')
                    if (k + 1 < cbuf.Length)
                    {
                        if (cbuf[k + 1] != '\n')
                            nrSep++;
                    }
                    else nrSep++;
                if (cbuf[k] == '\n')
                    nrSep++;
                if (nrSep < 2)
                    k--;
            }
            if (attemptPos + k == 0)
                TopPosition = 0;
            else
                TopPosition = attemptPos + (k + 1) * fileRead.CHAR_WIDTH;
            fileRead.Position = topPosition;
        }

        private void SetPosition(long topPos)
        {
            topPos = (topPos / fileRead.CHAR_WIDTH) * fileRead.CHAR_WIDTH;

            TopPosition = topPos;
            fileRead.Position = topPosition;
            if (topPos > 0)
            {
                int np;
                ReadLine(out np);
                TopPosition = np;
            }
        }

        void fileRead_SaveProgress(object sender, int progress)
        {
            if (this.SaveProgress != null)
                SaveProgress(sender, progress);
        }

        public void SynchDifferences()
        {
            int stLine = 0;
            int enLineRTB = rtb.Lines.Length - 1;
            int enLineCached = NUMBER_CACHED_LINES - 1;

            while (stLine < NUMBER_CACHED_LINES && stLine < rtb.Lines.Length)
            {
                if (lineLengths[stLine] == 0)
                    break;

                String fs = rtb.Lines[stLine];
                String os = new string(displayedLines[stLine], 0, lineLengths[stLine]);


                if (os.EndsWith("\r\n"))
                    os = os.Substring(0, os.Length - 2);
                else
                    if (os.EndsWith("\r") || os.EndsWith("\n"))
                        os = os.Substring(0, os.Length - 1);

                if (fs.Equals(os))
                {
                    stLine++;
                }
                else
                    break;
            }
            if (stLine == NUMBER_CACHED_LINES) //No changes done
                return;

            while (enLineCached > stLine)
            {
                String fs = rtb.Lines[enLineRTB];
                String os = new string(displayedLines[enLineCached], 0, lineLengths[enLineCached]);
                if (os.EndsWith("\r\n"))
                    os = os.Substring(0, os.Length - 2);
                else
                    if (os.EndsWith("\r") || os.EndsWith("\n"))
                        os = os.Substring(0, os.Length - 1);
                if (fs.Equals(os))
                {
                    enLineRTB--;
                    enLineCached--;
                }
                else
                    break;
            }

            string changed = "";
            int stOff = 0, len = 0;
            for (int i = 0; i <= enLineCached || i <= enLineRTB; ++i)
            {
                if (i < stLine)
                    stOff += lineLengths[i] == 0 ? 1 : lineLengths[i];


                String endLines = "\n";

                if (i >= stLine && i <= enLineCached)
                {
                    len += lineLengths[i];
                    String orig = new string(displayedLines[i], 0, lineLengths[i]);
                    if (orig.EndsWith("\r\n"))
                        endLines = "\r\n";
                    else
                        if (orig.EndsWith("\r") || orig.EndsWith("\n"))
                            endLines = "" + orig[orig.Length - 1];
                }
                if (i >= stLine && i <= enLineRTB)
                {
                    changed += rtb.Lines[i];
                    changed += endLines;
                }

            }


            Deletion del = new Deletion((int)(topPositionBeforeScroll + stOff * fileRead.CHAR_WIDTH), len * fileRead.CHAR_WIDTH);
            Insertion ins = new Insertion((int)(topPositionBeforeScroll + stOff * fileRead.CHAR_WIDTH), changed, changed.Length * fileRead.CHAR_WIDTH);



            if (len > 0)
                fileRead.AddDeletion(del);
            if (changed.Length > 0)
                fileRead.AddInsertion(ins);

            if (stLine == 0)
            {
                long topPos = topPosition + (changed.Length - len) * fileRead.CHAR_WIDTH;
                fileRead.Position = topPos < 0 ? 0 : topPos;
                topPosition = fileRead.Position;
            }
            vscroll.Maximum = (int)fileRead.Length;
        }
        #endregion

        #region Rich Text Box and Scroll Bar event handlers
        private void rtb_TextChanged(object sender, EventArgs e)
        {
            if (suspendTextChanged)
                return;

            dirty = true;

            if (fileInitialized)
                if (rtb.Lines.Length < NUMBER_CACHED_LINES)
                {
                    bool saveSuspend = suspendTextChanged;
                    suspendTextChanged = true;
                    topPositionBeforeScroll = topPosition;
                    SynchDifferences();
                    dirty = false;
                    int saveSel = rtb.SelectionStart;
                    fileRead.Position = topPosition;
                    CacheLines();
                    RefreshText();
                    rtb.SelectionStart = saveSel;
                    suspendTextChanged = saveSuspend;
                }
        }

        private void rtb_SizeChanged(object sender, EventArgs e)
        {
            InitializeLines();
            if (fileInitialized)
            {
                fileRead.Position = topPosition;
                CacheLines();
                RefreshText();
            }

        }

        void rtb_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta < 0)
            {
                if (topPosition < fileRead.Length)
                {
                    suspendTextChanged = true;
                    topPositionBeforeScroll = topPosition;
                    AdvanceOneLine();
                    CacheLines();
                    RefreshText();
                    suspendTextChanged = false;
                }
            }
            else
                if (topPosition > 0)
                {
                    suspendTextChanged = true;
                    topPositionBeforeScroll = topPosition;
                    BackOneLine();
                    CacheLines();
                    RefreshText();
                    suspendTextChanged = false;
                }
        }

        private void rtb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up)
            {
                if (rtb.GetLineFromCharIndex(rtb.SelectionStart) == 0)
                {
                    // We're on the first line and pressed up
                    topPositionBeforeScroll = topPosition;
                    if (topPosition > 0)
                    {
                        BackOneLine();
                        CacheLines();
                        RefreshText();
                    }
                }
            }

            if (e.KeyCode == Keys.PageUp)
            {
                if (rtb.GetLineFromCharIndex(rtb.SelectionStart) < 3)
                {
                    topPositionBeforeScroll = topPosition;
                    if (topPosition > 0)
                    {
                        BackOneLine();
                        CacheLines();
                        RefreshText();
                    }
                }
            }
            if (e.KeyCode == Keys.Down)
            {
                topPositionBeforeScroll = topPosition;
                int crtLnIdx = rtb.GetFirstCharIndexOfCurrentLine();
                int crtLn = rtb.GetLineFromCharIndex(crtLnIdx);


                if (crtLn >= NUMBER_CACHED_LINES - 1)
                {
                    AdvanceOneLine();
                    CacheLines();
                    RefreshText();
                    rtb.SelectionStart = rtb.GetFirstCharIndexFromLine(crtLn);
                }
            }
            if (e.KeyCode == Keys.PageDown)
            {
                topPositionBeforeScroll = topPosition;
                int crtLnIdx = rtb.GetFirstCharIndexOfCurrentLine();
                int crtLn = rtb.GetLineFromCharIndex(crtLnIdx);

                if (crtLn >= NUMBER_CACHED_LINES - 1)
                {
                    AdvanceOneLine();
                    CacheLines();
                    RefreshText();
                    rtb.SelectionStart = rtb.GetFirstCharIndexFromLine(crtLn);
                }
            }
        }

        private void vscroll_Scroll(object sender, ScrollEventArgs e)
        {

            if (!fileInitialized)
                return;
            topPositionBeforeScroll = topPosition;

            if (e.NewValue == e.OldValue)
                return;

            if (e.NewValue - e.OldValue == vscroll.SmallChange)
            {
                AdvanceOneLine();

            }
            else
                if (e.OldValue - e.NewValue == vscroll.SmallChange)
                {
                    BackOneLine();
                }
                else
                {
                    SetPosition(e.NewValue);
                }

            suspendTextChanged = true;
            CacheLines();
            RefreshText();

            suspendTextChanged = false;
        }
        #endregion

        #region Public methods
        public void LoadFile(String path)
        {
            this.filePath = path;

            try
            {
                fileRead = new RevisionStream(path);
                TopPosition = 0;
                vscroll.Maximum = (int)fileRead.Length;
                vscroll.Value = 0;
                fileRead.Position = 0;

                CacheLines();
                RefreshText();
                fileInitialized = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
                //Silent fail
            }
        }
        public void SaveFile(string path)
        {
            SynchDifferences();
            fileRead.SaveProgress += new ProgressChanged(fileRead_SaveProgress);
            fileRead.SaveTo(path);
        }

        public long TopPosition
        {
            get { return topPosition; }
            set
            {
                topPosition = value;
                vscroll.Value = (int)topPosition;
                if (PositionChanged != null)
                {
                    int pos = (int)(topPosition * 100 / fileRead.Length);
                    PositionChanged(this, pos + "% (position " + topPosition.ToString("#,0") + " of " + fileRead.Length.ToString("#,#") + ")");
                }
            }
        }

        public void Search(String term)
        {
            currentSearchPosition = topPosition;
            currentSearchTerm = term;

            new Thread(new ParameterizedThreadStart(SearchThread)).Start(this);
        }

        private void SearchThread(object sender)
        {
            long crtPos = currentSearchPosition;

            while (crtPos < fileRead.Length)
            {
                byte[] buffer = new byte[1024 * 1024];
                int rd = fileRead.Read(buffer, (int)crtPos, 1024 * 1024);

                char[] cbuf = fileRead.DetectedEncoding.GetChars(buffer);
                String s = new string(cbuf);
                int idx = s.IndexOf(currentSearchTerm);
                if (idx >= 0)
                {
                    currentSearchPosition = crtPos + idx;
                    if (SearchTermFound != null)
                    {
                        SearchTermFound(this, currentSearchPosition);

                        this.Invoke((MethodInvoker)delegate()
                        {
                            TopPosition = currentSearchPosition;
                            if (TopPosition > 0)
                            {
                                BackOneLine();
                                CacheLines();
                                RefreshText();

                                int visPos = rtb.Text.IndexOf(currentSearchTerm);
                                if (visPos >= 0)
                                {
                                    rtb.SelectionStart = visPos;
                                    rtb.SelectionLength = currentSearchTerm.Length;
                                    rtb.Focus();
                                }
                            }
                        });

                    }
                    break;
                }
                else
                {
                    crtPos += rd;
                    if (SearchProgress != null)
                        SearchProgress(this, (int)((crtPos * 100) / fileRead.Length));
                    if (crtPos >= fileRead.Length)
                        break;
                }

            }
        }

        public delegate void PositionChangeHandler(object sender, String pos);
        public event PositionChangeHandler PositionChanged;

        public delegate void SearchProgressHandler(object sender, int percent);
        public event SearchProgressHandler SearchProgress;
        public delegate void SearchFoundHandler(object sender, long position);
        public event SearchFoundHandler SearchTermFound;

        public event ProgressChanged SaveProgress;
        #endregion


    }
}
