﻿/*
 * Disclaimer: Sample Code is provided for the purpose of illustration only and is not intended to be used in 
 * a production environment. 
 * THIS SAMPLE CODE AND ANY RELATED INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
 * EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND/OR FITNESS FOR A PARTICULAR PURPOSE. We grant You a nonexclusive, royalty-free right to use and modify the 
 * Sample Code and to reproduce and distribute the object code form of the Sample Code, provided that. 
 * You agree: (i) to not use Our name, logo, or trademarks to market Your software product in which the Sample Code 
 * is embedded; (ii) to include a valid copyright notice on Your software product in which the Sample Code is embedded; 
 * and (iii) to indemnify, hold harmless, and defend Us and Our suppliers from and against any claims or lawsuits, 
 * including attorneys’ fees, that arise or result from the use or distribution of the Sample Code 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace LFETester
{
    class RevisionStream
    {
        private FileStream str;
        private List<Segment> segments;
        private long position;
        private long realLength;
        public event ProgressChanged SaveProgress;
        public Encoding DetectedEncoding;
        public int CHAR_WIDTH;

        /// <summary>
        /// RevisionStream Constructor
        /// </summary>
        /// <param name="path">Path to file being edited</param>
        public RevisionStream(String path)
        {
            Logging.Initialize();
            str = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 4096, false);
            segments = new List<Segment>();
            segments.Add(new Segment(0, (int)str.Length - 1));
            position = 0;
            realLength = str.Length;
            DetectEncoding();
        }

        private void DisplaySegs()
        {
            String st = "";
            for (int i = 0; i < segments.Count; ++i)
            {
                st = st + "[" + segments[i].start + ", " + segments[i].end + " (" + segments[i].data + ")] ";
            }
            Logging.Log(st);
        }

        #region Public methods
        /// <summary>
        /// Signal a deletion
        /// </summary>
        /// <param name="del"></param>
        public void AddDeletion(Deletion del)
        {
            Logging.Log("Deletion at: " + del.offset + " len= " + del.Length);
            AdjustForDeletion(segments, del);
            DisplaySegs();
            realLength -= del.Length;
        }

        /// <summary>
        /// Signal an insertion
        /// </summary>
        /// <param name="ins"></param>
        public void AddInsertion(Insertion ins)
        {
            Logging.Log("Insertion at: " + ins.offset + " len= " + ins.Length);
            AdjustForInsertion(segments, ins);
            DisplaySegs();
            realLength += ins.Length;
        }

        /// <summary>
        /// Read from stream + changes len bytes starting at offset
        /// </summary>
        /// <param name="buf">Output array</param>
        /// <param name="offset">Offset in changed stream to read from</param>
        /// <param name="len">Number of bytes to read</param>
        /// <returns></returns>
        public int Read(byte[] buf, int offset, int len)
        {

            if (offset >= this.Length)
                return -1;

            if (offset < 0)
                offset = 0;

            if (offset + len >= this.Length)
                len = (int)(this.Length - offset);

            Recompose(buf, offset, len);
            int ret = len;
            if (this.Length - offset < ret)
                ret = (int)this.Length - offset;

            return ret;
        }

        /// <summary>
        /// Save changed stream to file
        /// </summary>
        /// <param name="filePath">Path of file where changed stream will be saved</param>
        public void SaveTo(String filePath)
        {
            long crtPos = 0;
            //Write chunks of 1MB
            int bytesAtOnce = 1 * 1024 * 1024;
            byte[] buf = new byte[bytesAtOnce];

            BinaryWriter sw = new BinaryWriter(new FileStream(filePath, FileMode.Create));

            while (crtPos < realLength)
            {
                int toRead = bytesAtOnce;
                if (crtPos + toRead >= realLength)
                    toRead = (int)(realLength - crtPos);

                Recompose(buf, (int)crtPos, toRead);
                sw.Write(buf, 0, toRead);
                crtPos += toRead;

                if (SaveProgress != null)
                {
                    int percent = (int)(crtPos * 100 / realLength);
                    SaveProgress(this, percent);
                }
            }
            sw.Close();
        }
        #endregion

        #region Properties
        public long Position
        {
            get { return position; }
            set { position = value; }
        }

        public long Length
        {
            get { return realLength; }
        }
        #endregion

        private void DetectEncoding()
        {
            if (str.Length < 4)
            {
                DetectedEncoding = new ASCIIEncoding();
                CHAR_WIDTH = 1;
            }
            else
            {
                byte[] chars = new byte[4];
                str.Read(chars, 0, 4);
                CHAR_WIDTH = 2;
                
                if (chars[0] == 0xFF && chars[1] == 0xFE)
                    DetectedEncoding = new UnicodeEncoding();
                else
                    if (chars[3] == 0)
                        DetectedEncoding = new UnicodeEncoding(true, false);
                    else
                    {
                        if(chars[0] == 0xEF && chars[1] == 0xBB && chars[2] == 0xBF){
                               CHAR_WIDTH = 1;
                        DetectedEncoding = new UTF8Encoding(true);
                        }
                        else{
                        CHAR_WIDTH = 1;
                        DetectedEncoding = new ASCIIEncoding();
                        }
                    }
            }
        }

        /// <summary>
        /// Copy len bytes from changed stream to buf starting at offset in the changed stream
        /// </summary>
        /// <param name="buf"></param>
        /// <param name="offset"></param>
        /// <param name="len"></param>
        private void Recompose(byte[] buf, int offset, int len)
        {
            int stOff = 0, offCrt = 0;
            int i = 0;


            while (i < segments.Count && offCrt < offset)
            {
                stOff = offCrt;

                offCrt += segments[i].Lenght;
                if (offCrt < offset)
                    i++;
            }
            if (offset == 0)
                offCrt = segments[0].Lenght;

            int crt = 0;

            if (offset + len <= offCrt) //Read only from first segment
            {
                if (segments[i].fromOriginal)
                {
                    byte[] temp = new byte[offCrt - offset + 1];
                    int start = segments[i].start + (offset - stOff);
                    if (segments[i].start == -1)
                        start++;

                    str.Position = start;
                    str.Read(temp, 0, len);
                    Array.Copy(temp, buf, len);
                }
                else
                {
                    byte[] str = DetectedEncoding.GetBytes(segments[i].data.ToCharArray());
                    Array.Copy(str, offset - stOff, buf, 0, len);
                }
            }
            else
            {
                //Read past first

                int toRead = len;

                if (segments[i].fromOriginal)
                {
                    byte[] temp = new byte[offCrt - offset];
                    int start = segments[i].start + (offset - stOff);

                    str.Position = start;
                    str.Read(temp, 0, offCrt - offset);
                    Array.Copy(temp, buf, offCrt - offset);
                }
                else
                {
                    byte[] str = DetectedEncoding.GetBytes(segments[i].data.ToCharArray());
                    Array.Copy(str, offset - stOff, buf, 0, offCrt - offset);
                }
                toRead -= (offCrt - offset);
                crt += (offCrt - offset);
                i++;

                while (i < segments.Count && toRead > 0)
                {
                    if (segments[i].Lenght > toRead)
                    {

                        if (segments[i].fromOriginal)
                        {
                            byte[] temp = new byte[toRead];
                            int start = segments[i].start;

                            str.Position = start;
                            str.Read(temp, 0, toRead);
                            Array.Copy(temp, 0, buf, crt, toRead);
                        }
                        else
                        {
                            byte[] str = DetectedEncoding.GetBytes(segments[i].data.ToCharArray());
                            Array.Copy(str, 0, buf, crt, toRead);
                        }
                        toRead = 0;
                    }
                    else
                    {
                        if (segments[i].fromOriginal)
                        {
                            byte[] temp = new byte[segments[i].Lenght];
                            int start = segments[i].start;

                            str.Position = start;
                            str.Read(temp, 0, segments[i].Lenght);
                            Array.Copy(temp, 0, buf, crt, segments[i].Lenght);
                        }
                        else
                        {
                            byte[] str = DetectedEncoding.GetBytes(segments[i].data.ToCharArray());
                            Array.Copy(str, 0, buf, crt, segments[i].Lenght);
                        }
                        crt += segments[i].Lenght;
                        toRead -= segments[i].Lenght;
                        i++;
                    }

                }

            }
        }


        /// <summary>
        /// Adjusts list of segments with an Insertion
        /// </summary>
        /// <param name="olist"></param>
        /// <param name="d"></param>
        private void AdjustForInsertion(List<Segment> olist, Insertion d)
        {
            int stOff = 0, offCrt = 0;
            int i = 0;

            while (i < olist.Count && offCrt < d.offset)
            {
                stOff = offCrt;
                offCrt += olist[i].end - olist[i].start + 1;
                if (offCrt < d.offset)
                    i++;
            }

            //Segment between stOff and offCrt (real offsets)
            Segment seg = new Segment(0, d.length - 1);
            seg.fromOriginal = false;
            seg.data = d.addedString;

            if (d.offset == stOff)
            {
                olist.Insert(i, seg);
            }
            else
            {
                if (i >= olist.Count)
                {
                    olist.Add(seg);
                }
                else
                {
                    if (olist[i].fromOriginal)
                    {
                        int len = olist[i].Lenght;
                        int exEnd = olist[i].end;
                        olist[i].end = olist[i].start + d.offset - stOff - 1;
                        if (olist[i].start == -1)
                            olist[i].end++;

                        olist.Insert(i + 1, seg);
                        if (len - d.offset + stOff > 0)
                        {
                            olist.Insert(i + 2, new Segment((olist[i].start == -1 ? 0 : olist[i].start) + d.offset - stOff, exEnd));
                        }
                    }
                    else
                    {
                        int len = olist[i].Lenght;
                        olist[i].end = olist[i].start + d.offset - stOff - 1;
                        String origData = olist[i].data;

                        olist[i].data = olist[i].data.Substring(olist[i].start, (olist[i].start + d.offset - stOff) / CHAR_WIDTH);
                        olist.Insert(i + 1, seg);
                        if (len - d.offset + stOff > 0)
                        {
                            Segment ns = new Segment(0, len - d.offset + stOff);
                            ns.fromOriginal = false;
                            ns.data = origData.Substring((d.offset - stOff) / CHAR_WIDTH);
                            olist.Insert(i + 2, ns);
                        }
                    }
                }
            }



        }

        /// <summary>
        /// Adjusts list of Segments with a Deletion
        /// </summary>
        /// <param name="olist"></param>
        /// <param name="d"></param>
        private void AdjustForDeletion(List<Segment> olist, Deletion d)
        {
            if (olist.Count == 1)
            {
                if (d.offset == 0 && d.length >= olist[0].Lenght) //Delete all
                {
                    olist.Clear();
                }
                else
                {
                    if (olist[0].Lenght <= d.offset + d.length)
                    { //Delete to end
                        if (!olist[0].fromOriginal)
                        {
                            olist[0].end = d.offset - 1;
                            olist[0].data = olist[0].data.Substring(0, olist[0].Lenght / CHAR_WIDTH);
                        }
                        else
                        {
                            olist[0].end = d.offset - 1;
                        }

                    }
                    else
                    {
                        //Split in two
                        if (!olist[0].fromOriginal)
                        {

                            String data = olist[0].data;
                            int len = olist[0].Lenght;
                            int oldStart = olist[0].start;

                            olist[0].end = olist[0].end - d.offset - d.length;
                            olist[0].data = data.Substring((d.offset + d.length) / CHAR_WIDTH);

                            if (d.offset > 0)
                            {
                                Segment ns = new Segment(0, d.offset - 1);
                                ns.data = data.Substring(0, ns.Lenght / CHAR_WIDTH);
                                ns.fromOriginal = false;
                                olist.Insert(0, ns);
                            }
                        }
                        else
                        {
                            int oldStart = olist[0].start;
                            olist[0].start = d.offset + d.length;
                            if (d.offset > 0)
                                olist.Insert(0, new Segment(oldStart, d.offset - 1));
                        }
                    }
                }
                return;
            }
            int stOff = 0, offCrt = 0;
            int i = 0;

            while (i < olist.Count && offCrt <= d.offset)
            {
                stOff = offCrt;
                offCrt += olist[i].end - olist[i].start + 1;
                if (offCrt <= d.offset)
                    i++;
            }

            //Segment between stOff and offCrt (real offsets)
            int toDelete = d.length;

            if (toDelete <= offCrt - d.offset) //Delete only from first segment
            {//eg: [0,4], delete offset 2, len = 1,2 or 3
                if (d.offset == stOff) //eg: [0,4], delete 0, len = 1, 2, 3, 4, 5
                {
                    if (olist[i].fromOriginal)
                    {
                        olist[i].start += toDelete;
                        if (olist[i].start > olist[i].end)
                            olist.RemoveAt(i);
                    }
                    else
                    {
                        int origLen = olist[i].Lenght;

                        olist[i].end -= (toDelete - 1) * CHAR_WIDTH;
                        if (olist[i].end <= 0)
                            olist.RemoveAt(i);
                        else
                            olist[i].data = olist[i].data.Substring(toDelete, (origLen - toDelete) / CHAR_WIDTH);
                    }

                }
                else
                {//eg: [0,4], delete 1, len = 1,2,3,4
                    int exEnd = olist[i].end;
                    String data = olist[i].data;
                    int exLen = olist[i].Lenght;

                    olist[i].end = olist[i].start + (d.offset - stOff) - 1;
                    if (!olist[i].fromOriginal)
                    {
                        olist[i].data = data.Substring(0, (d.offset - stOff) / CHAR_WIDTH);
                    }

                    if (exLen - d.length - olist[i].Lenght > 0)
                    {
                        Segment rest;

                        if (!olist[i].fromOriginal)
                        {
                            rest = new Segment(0, exLen - d.length - olist[i].Lenght - 1);
                            rest.fromOriginal = false;
                            rest.data = data.Substring((d.offset - stOff + d.length) / CHAR_WIDTH);
                        }
                        else
                        {
                            rest = new Segment(olist[i].start + d.offset - stOff + d.length, exEnd);
                        }
                        olist.Insert(i + 1, rest);
                    }
                }
            }
            else //Delete past first segment
            {
                toDelete -= (offCrt - d.offset);

                int exEnd = olist[i].end;
                String data = olist[i].data;
                int exLen = olist[i].Lenght;
                olist[i].end = olist[i].start + d.offset - stOff - 1;

                if (!olist[i].fromOriginal)
                {
                    olist[i].data = data.Substring(0, olist[i].Lenght / CHAR_WIDTH);
                }

                if (olist[i].end < olist[i].start)
                    olist.RemoveAt(i);
                else
                    i++;



                while (i < olist.Count && toDelete > 0)
                {
                    if (olist[i].Lenght <= toDelete)
                    {
                        toDelete -= olist[i].Lenght;
                        olist.RemoveAt(i);
                    }
                    else
                    {

                        if (!olist[i].fromOriginal)
                        {
                            olist[i].end -= toDelete;
                            olist[i].data = olist[i].data.Substring(toDelete / CHAR_WIDTH);
                        }
                        else
                        {
                            olist[i].start += toDelete;
                        }
                        toDelete = 0;
                    }
                }
            }

        }

    }

    public delegate void ProgressChanged(object sender, int progress);

    class Segment : IComparable
    {
        public int start, end;
        public bool fromOriginal;
        public String data;

        public Segment(int st, int en)
        {
            start = st;
            end = en;
            fromOriginal = true;
            data = null;
        }

        public override String ToString()
        {
            return "[" + start + "," + end + "]";
        }

        public int Lenght
        {
            get
            {
                if (this.start == -1)
                    return this.end + 1;
                else
                    return this.end - this.start + 1;
            }
        }

        public static bool operator ==(Segment s1, Segment s2)
        {
            if (s1.start == s2.start && s1.end == s2.end)
            {
                return true;
            }
            else
                return false;
        }
        public static bool operator !=(Segment s1, Segment s2)
        {
            if (s1.start != s2.start || s1.end != s2.end)
            {
                return true;
            }
            else
                return false;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Segment))
                throw new NotImplementedException();

            Segment s1, s2;
            s1 = this;
            s2 = obj as Segment;
            if (s1.start == s2.start && s1.end == s2.end)
            {
                return true;
            }
            else
                return false;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public int CompareTo(object obj)
        {
            if (!(obj is Segment))
                throw new NotImplementedException();

            Segment s = obj as Segment;

            if (s.start < this.start && s.end > this.start)
                throw new NotImplementedException();

            if (s.end > this.end && s.start < this.end)
                throw new NotImplementedException();

            return this.start - s.start;
        }

    }

    class Diff
    {
        public int offset, length;

        public Diff(int offset, int len)
        {
            this.offset = offset;
            this.length = len;
        }

        public int Length
        {
            get { return length; }

        }
    }

    class Insertion : Diff
    {
        public String addedString;

        public Insertion(int offset, String added)
            : base(offset, added.Length)
        {
            addedString = added;
        }
        public Insertion(int offset, String added, int realLength)
            : base(offset, realLength)
        {
            addedString = added;
        }

    }
    class Deletion : Diff
    {
        public Deletion(int offset, int len)
            : base(offset, len)
        {
        }
    }
}
